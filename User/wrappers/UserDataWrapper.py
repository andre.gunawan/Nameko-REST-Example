from exceptions import NotFound

class UserDataWrapper:

    NotFound = NotFound

    def __init__(self, connection_pool):
        self.connection_pool = connection_pool
        self.connection = self.connection_pool.get_connection()

    def login_user(self, username, password):
        cursor = self.connection.cursor(dictionary=True)

        sql = '''
            SELECT * FROM user
            WHERE username = ? AND password = ? AND status = 'ACTIVE'
        '''

        cursor.execute(sql, (username, password,))

        status = 1
        record = cursor.fetchone()
        if not record:
            status = 0
        
        return {
            'status': status,
            'data': record
        }

    def get_all_available_user(self):
        cursor = self.connection.cursor(dictionary=True)

        sql = '''
            SELECT * FROM user
            WHERE status = 'ACTIVE'
        '''

        cursor.execute(sql)
        rows = cursor.fetchall()
        return rows

    def get_user_by_id(self, id):
        cursor = self.connection.cursor(dictionary=True)

        sql = '''
            SELECT * FROM user
            WHERE status = 'ACTIVE' AND id = ?
        '''

        cursor.execute(sql, (id,))
        row = cursor.fetchone()

        if not row:
            raise NotFound('User with ID: {} Not Found'.format(str(id)))

        return row

    def add_user(self, data):
        cursor = self.connection.cursor()

        sql = '''
            INSERT INTO user VALUES (default, ?, ?, default, default)
        '''

        cursor.execute(sql, (data['username'], data['password'],))
        id_of_new_row = cursor.lastrowid
        return id_of_new_row

    def update_user(self, id, data):
        cursor = self.connection.cursor(dictionary=True)

        sql = '''
            UPDATE user
            SET  username = ?, password = ?
            WHERE id = ?
        '''

        cursor.execute(sql, (data['username'], data['password'], id,))

    def delete_user(self, id):
        cursor = self.connection.cursor()

        sql = '''
            UPDATE user
            SET status = 'DELETED'
            WHERE id = ?
        '''

        cursor.execute(sql , (id,))

    def __del__(self):
        self.connection_pool.putconn(self.connection)
