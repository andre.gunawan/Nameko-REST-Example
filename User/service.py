from nameko.rpc import rpc

from providers.DataProvider import DataProvider

class UserService:
    name = "user_service"

    data_provider = DataProvider()

    @rpc
    def hello(self, name):
        return "Hello, {}!".format(name)
    

    @rpc
    def login_user(self, username, password):
        return self.data_provider['user'].login_user(username, password)

    @rpc
    def get_all_available_user(self):
        return self.data_provider['user'].get_all_available_user()

    @rpc
    def get_user_by_id(self, id):
        return self.data_provider['user'].get_user_by_id(id)

    @rpc
    def add_user(self, data):
        return self.data_provider['user'].add_user(data)
    
    @rpc
    def update_user(self, id, data):
        self.data_provider['user'].update_user(id, data)
        return self.data_provider['user'].get_user_by_id(id)
    
    @rpc
    def delete_user(self, id):
        self.data_provider['user'].delete_user(id)