import mariadb
import configparser
from nameko.extensions import DependencyProvider

from wrappers.UserDataWrapper import UserDataWrapper

class DataProvider(DependencyProvider):

    def setup(self):
        config = configparser.ConfigParser()
        config.read('config.ini')
        db_config = config['database']

        self.pool = mariadb.ConnectionPool(
            host="localhost", 
            port=3306,
            user=db_config['username'], 
            password=db_config['password'],
            database=db_config['database'],
            pool_name="user-service-pool", 
            pool_size=20,
            autocommit=True
        )

        self.available_data = {
            'user': UserDataWrapper(self.pool)
        }
    
    def get_dependency(self, worker_ctx):
        return self.available_data
