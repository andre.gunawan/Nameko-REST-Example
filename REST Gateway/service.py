import json
from urllib import response

from nameko.exceptions import BadRequest
from nameko.rpc import RpcProxy
from werkzeug import Response

from entrypoints import http
from exceptions import UserNotFound, NoteNotFound
from providers.SessionProvider import SessionProvider

class RESTGatewayService(object):

    name = 'rest_gateway_service'

    user_rpc = RpcProxy('user_service')
    note_rpc = RpcProxy('note_service')

    session_provider = SessionProvider()

    # For Testing Purpose

    @http("GET", "/api/users")
    def get_all_available_user(self, request):
        users = self.user_rpc.get_all_available_user()
        return Response(
            json.dumps(users),
            mimetype='application/json'
        )

    # On Work

    @http("POST", "/api/login")
    def login_user(self, request):
        data = request.json
        login_status = self.user_rpc.login_user(data['username'], data['password'])

        result = {
            'status': 1,
            'msg': ''
        }
        if login_status['status'] == 1:
            session_id = self.session_provider.set_session(login_status['data'])
            result['msg'] = 'Success'
        else:
            result['status'] = 0
            result['msg'] = 'Wrong Username or Password'

        response = Response(
            json.dumps(result),
            mimetype='application/json'
        )

        if login_status['status'] == 1:
            response.set_cookie('session_id', session_id)
        
        return response
