from nameko.rpc import rpc

from providers.DataProvider import DataProvider

class NoteService:
    name = "note_service"

    data_provider = DataProvider()

    @rpc
    def hello(self, name):
        return "Hello, {}!".format(name)
    
    @rpc
    def get_all_user_note(self, user_id):
        return self.data_provider['note'].get_all_user_note(user_id)

    @rpc
    def get_note_by_id(self, id):
        return self.data_provider['note'].get_note_by_id(id)

    @rpc
    def add_note(self, user_id, data):
        return self.data_provider['note'].add_note(user_id, data)
    
    @rpc
    def update_note(self, id, data):
        self.data_provider['note'].update_note(id, data)
        return self.data_provider['note'].get_note_by_id(id)
    
    @rpc
    def delete_note(self, id):
        self.data_provider['note'].delete_note(id)