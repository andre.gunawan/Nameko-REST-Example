from exceptions import NotFound

class NoteDataWrapper:

    NotFound = NotFound

    def __init__(self, connection_pool):
        self.connection_pool = connection_pool
        self.connection = self.connection_pool.get_connection()

    def get_all_user_note(self, user_id):
        cursor = self.connection.cursor(dictionary=True)

        sql = '''
            SELECT * FROM note
            WHERE status = 'ACTIVE' AND user_id = ?
        '''

        cursor.execute(sql, (user_id,))
        rows = cursor.fetchall()
        if not rows:
            rows = []
        return rows

    def get_note_by_id(self, id):
        cursor = self.connection.cursor(dictionary=True)

        sql = '''
            SELECT * FROM note
            WHERE status = 'ACTIVE' AND id = ?
        '''

        cursor.execute(sql, (id,))
        row = cursor.fetchone()

        if not row:
            raise NotFound('Note with ID: {} Not Found'.format(str(id)))

        return row

    def add_note(self, user_id, data):
        cursor = self.connection.cursor()

        sql = '''
            INSERT INTO note VALUES (default, ?, ?, ?, default, default)
        '''

        cursor.execute(sql, (user_id, data['title'], data['content'],))
        id_of_new_row = cursor.lastrowid
        return id_of_new_row

    def update_note(self, id, data):
        cursor = self.connection.cursor(dictionary=True)

        sql = '''
            UPDATE note
            SET  title = ?, content = ?
            WHERE id = ?
        '''

        cursor.execute(sql, (data['title'], data['content'], id,))

    def delete_note(self, id):
        cursor = self.connection.cursor()

        sql = '''
            UPDATE note
            SET status = 'DELETED'
            WHERE id = ?
        '''

        cursor.execute(sql , (id,))

    def __del__(self):
        self.connection_pool.putconn(self.connection)
